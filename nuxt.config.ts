import vuetify, { transformAssetUrls } from 'vite-plugin-vuetify'
// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
  devtools: { enabled: true },
  build: {
    transpile: ['vuetify']
  },
  runtimeConfig: {
    NODE_ENV: process.env.NODE_ENV
  },
  modules: [
    (_options, nuxt) => {
      nuxt.hooks.hook('vite:extendConfig', config => {
        config.plugins.push(vuetify({ autoImport: true }))
      })
    },
    '@pinia/nuxt',
    '@pinia-plugin-persistedstate/nuxt'
  ],
  vite: {
    vue: {
      template: {
        transformAssetUrls
      }
    },
    server: {
      proxy: {
        '/api': {
          target: process.env.API_BASE_URL,
          changeOrigin: true,
          rewrite: (path: string) => path.replace(/^\/api/, '')
        }
      }
    }
  },
  plugins: [
    '~/plugins/vuei18n.js',
    '~/plugins/vuetify.js',
    '~/plugins/constantsFromDB.js',
    '~/plugins/components/index.js'
  ]
})
