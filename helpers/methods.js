export const cfObjectAssign = function (obj) {
  if (typeof obj !== 'object' || obj === null || obj instanceof Date) {
    return obj
  } else if (Array.isArray(obj)) {
    const rtrn = []
    obj.forEach((elem, index) => {
      rtrn[index] = cfObjectAssign(elem)
    })
    return rtrn
  } else {
    const rtrn = {}
    for (const [key, value] of Object.entries(obj)) {
      rtrn[key] = cfObjectAssign(value)
    }
    return rtrn
  }
}

export const getDateTimePart = function (dateTime, part) {
  dateTime = new Date(dateTime)
  const parts = {
    fullYear: dateTime.getFullYear(),
    year: dateTime.getFullYear() % 100,
    month: ('0' + (dateTime.getMonth() + 1)).slice(-2),
    day: ('0' + dateTime.getDate()).slice(-2),
    hours: ('0' + dateTime.getHours()).slice(-2),
    minutes: ('0' + dateTime.getMinutes()).slice(-2),
    seconds: ('0' + dateTime.getSeconds()).slice(-2)
  }
  part = part.toLowerCase()
  if (part === 'dd') return parts.day
  if (part === 'mm') return parts.month
  if (part === 'yy') return parts.year
  if (part === 'yyyy') return parts.fullYear
  if (part === 'dd.mm') return parts.day + '.' + parts.month
  if (part === 'dd.mm.yy') return parts.day + '.' + parts.month + '.' + parts.year
  if (part === 'dd.mm.yyyy') return parts.day + '.' + parts.month + '.' + parts.fullYear
  if (part === 'yyyy-mm-dd') return parts.fullYear + '-' + parts.month + '-' + parts.day
  if (part === 'yy-mm-dd') return parts.year + '-' + parts.month + '-' + parts.day
  if (part === 'dd-mm-yy') return parts.day + '-' + parts.month + '-' + parts.year
  if (part === 'hh:mm:ss') return parts.hours + ':' + parts.minutes + ':' + parts.seconds
  if (part === 'hh:mm') return parts.hours + ':' + parts.minutes
  if (part === 'mm:ss') return parts.minutes + ':' + parts.seconds
  if (part === 'dd.mm.yy hh:mm')
    return parts.day + '.' + parts.month + '.' + parts.year + ' ' + parts.hours + ':' + parts.minutes
}
