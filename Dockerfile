FROM node:21.7.3

WORKDIR /app

COPY package.json /app

RUN npm install

COPY . /app

EXPOSE 9002

CMD ["npm", "run", "dev"]