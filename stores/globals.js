import { defineStore } from 'pinia'

export const useGLobalsStore = defineStore('globals', {
  state: () => {
    return {}
  }
})
