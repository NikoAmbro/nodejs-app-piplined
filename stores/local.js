import { defineStore } from 'pinia'

export const useLocalStore = defineStore('local', {
  state: () => ({
    locale: 'ka'
  }),
  persist: {
    storage: persistedState.localStorage
  }
})
