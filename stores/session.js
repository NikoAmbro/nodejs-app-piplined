import { defineStore } from 'pinia'

export const useSessionStore = defineStore('session', {
  state: () => ({}),
  persist: {
    storage: persistedState.sessionStorage
  }
})
