import { createI18n } from 'vue-i18n'
import { useLocalStore } from '~/stores/local'
import ka from '../locales/ka.json'
import en from '../locales/en.json'

export default defineNuxtPlugin(nuxtApp => {
  const localState = useLocalStore()
  const i18n = createI18n({
    locale: localState.locale,
    fallbackLocale: 'ka',
    messages: { ka, en }
  })
  nuxtApp.vueApp.use(i18n)
})
