import CfSpacer from './cfSpacer'
import CfDialogCloser from './cfDialogCloser'

const components = {
  CfSpacer,
  CfDialogCloser
}

export default defineNuxtPlugin(nuxtApp => {
  Object.entries(components).forEach(([name, component]) => {
    nuxtApp.vueApp.component(name, component)
  })
})
